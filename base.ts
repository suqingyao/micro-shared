/*
 * @Author: suxunying
 * @desc: 
 * @Date: 2021-07-26 09:23:45
 * @LastEditors: suxunying
 * @LastEditTime: 2021-07-28 15:35:29
 * @FilePath: \bmp-poc\plat\src\utils\shared\base.ts
 */
/*
    分享类（基本）
    整个shared的核心功能
*/
import { Store } from 'redux';
import { update } from './utils'


export default class BaseShared {
    static pool: Store;

    static actions = new Map();

    static cacheState: any = null;

    constructor(Pool: Store, action = new Map()) {
        BaseShared.pool = Pool;
        BaseShared.actions = action;
    }

    public init(listener: Function): void {
        BaseShared.cacheState = BaseShared.pool.getState();
        BaseShared.pool.subscribe(() => {
            const newState: any = BaseShared.pool.getState();
            const stateName = update(BaseShared.cacheState, newState);
            BaseShared.cacheState = newState;
            return listener(stateName);
        });
    }

    public dispatch(target: string, param?: any):any {
        const res:any = BaseShared.actions.get(target)(param ? param : null);
        return res;
    }
}

