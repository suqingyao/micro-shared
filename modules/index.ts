/*
 * @Author: suxunying
 * @desc: 
 * @Date: 2021-07-27 16:38:13
 * @LastEditors: suxunying
 * @LastEditTime: 2021-07-29 18:23:11
 * @FilePath: \bmp-poc\plat\src\utils\shared\modules\index.ts
 */
import createPool from './pool';
import createAction from './action';

export function flatModule(option: any) {
    const { modules } = option;
    const reducers: any = {}
    const actionList: Array<any> = [];
    Object.values(modules).forEach((obj: any) => {
        const { reducer, action, name } = obj;
        reducers[name] = reducer;
        actionList.push({
            name,
            actionObj: action,
        })
    })

    const pool = createPool(reducers);
    const actions = createAction(actionList, pool);
    
    return {
        pool,
        actions,
    }
}
