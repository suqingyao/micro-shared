/*
 * @Author: suxunying
 * @desc: reducer模块管理
 * @Date: 2021-07-21 10:05:28
 * @LastEditors: suxunying
 * @LastEditTime: 2021-07-29 18:23:31
 * @FilePath: \bmp-poc\plat\src\utils\shared\modules\pool.ts
 */
import { combineReducers, createStore } from 'redux';

function createPool(reducers = {}) {
    const staticReducers = combineReducers(reducers);
    return createStore(staticReducers);
}

export default createPool;
