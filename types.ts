/*
 * @Author: suxunying
 * @desc: 类型
 * @Date: 2021-07-27 15:19:02
 * @LastEditors: suxunying
 * @LastEditTime: 2021-07-29 17:59:18
 * @FilePath: \bmp-poc\plat\src\utils\shared\types.ts
 */
import BaseShared from './base';

import { Store } from 'redux';

type Mutation = {
    type: string;
    payload: any;
}

interface Pool extends Store{}

export {
    BaseShared,
    Pool,
    Mutation
}