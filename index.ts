/*
 * @Author: suxunying
 * @desc: main
 * @Date: 2021-07-27 15:07:49
 * @LastEditors: suxunying
 * @LastEditTime: 2021-07-30 10:12:14
 * @FilePath: \bmp-poc\micro-shared\index.ts
 */
import BaseShared from './base';
import { flatModule } from './modules';

class Shared {
    static shared:BaseShared;
    
    constructor(option = {}) {
        const { pool, actions } = flatModule(option);
        Shared.shared = new BaseShared(pool, actions);
    }

    public getShared() {
        return Shared.shared;
    }
}

export default Shared;

export * from './types';